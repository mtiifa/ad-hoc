package whoWillRemain;

public class WhoLivesTest {
	public static void main(String[] args) {
		WhoLives whoLives = new WhoLives(1, 100);
		int numberOfLastPerson = whoLives.process();
		System.out.println("The last person is #" + numberOfLastPerson);
	}
}
