package whoWillRemain;

public class WhoLives {

	int start, end;

	WhoLives(int start, int end) {
		this.start = start;
		this.end = end;
	}

	class person {
		int number;

		person(int number) {
			this.number = number;
		}
	}

	SinglyLinkedList list = new SinglyLinkedList();

	public int process() {

		// add 100 persons with their numbers into the list

		for (int i = start; i <= end; i++) {
			person x = new person(i);
			list.add(x);
		}

		// find the remaining person

		int counter = 0;
		while (list.size() > 1) {
			counter++;
			list.remove(counter);
			if (counter == list.size() - 1) {
				counter = -1;
			} else if (counter == list.size()) {
				counter = 0;
			}
		}

		int lastPerson = ((person) (list.get(0))).number;
		return lastPerson;
	}
}
