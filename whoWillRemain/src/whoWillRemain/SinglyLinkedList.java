package whoWillRemain;


public class SinglyLinkedList implements ILinkedList {

	/**
	 * class contains object and
	 * one references for
	 * next class.
	 */
	public class Node {

		private Object element;
		// Element stored in this node
		private Node next;
		// reference To the next node in the list

		/**
		 * @param o object.
		 * @param n reference.
		 */
		public Node(Object o, Node n) {
			element = o;
			next = n;
		}

		/** Returns the element of this node. */
		public Object getElement() {
			return element;
		}

		/** Returns the next node of this node. */
		public Node getNext() {
			return next;
		}

		// Modifier methods:
		/**
		 * Sets the element of this node.
		 * @param newElem a new element to be set.
		 */
		public void setElement(Object newElem) {
			element = newElem;
		}

		/**
		 * Sets the next node of this node.
		 * @param newNext change the next reference.
		 */
		public void setNext(Node newNext) {
			next = newNext;
		}
	}

	private Node head; // head node of the list
	private int size; // number of nodes in the list

	/** Returns the element of this node. */
	public Node getHead() {
		return head;
	}

	/** Default constructor that creates an empty list. */
	public SinglyLinkedList() {
		head = null;
		size = 0;
	}

	@Override
	public void add(Object element) {
		if (element != null) {
			if (isEmpty()) {
				Node newNode = new Node(element, null);
				head = newNode;
			} else {
				Node newNext = new Node(element, null);
				Node n = head;
				while (n.next != null) {
					n = n.next;
				}
				n.setNext(newNext);
			}
			size++;
		} else {
			throw null;
		}
	}

	@Override
	public Object get(int index) {
		if (index < size && index >= 0) {
			Node n = head;
			for (int i = 0; i < index; i++) {
				n = n.next;
			}
			return n.getElement();
		} else {
			throw null;
		}
	}

	@Override
	public boolean isEmpty() {
		return size == 0;
	}

	@Override
	public void remove(int index) {
		if (index < size && index >= 0) {
			if (index == 0) {
				head = head.next;
			} else {
				Node n = head;
				for (int i = 0; i < index - 1; i++) {
					n = n.next;
				}
				Node v = n;
				n = n.next.next;
				v.setNext(n);
			}
			size--;
		} else {
			throw null;
		}
	}

	@Override
	public int size() {
		return size;
	}
}
