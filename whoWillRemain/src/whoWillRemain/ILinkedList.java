package whoWillRemain;

/**
 * @author Ali
 */
public interface ILinkedList {

	/** Inserts the specified element at the end of the list. */
	public void add(Object element);

	/** Returns the element at the specified position in this list. */
	public Object get(int index);

	/** Returns true if this list contains no elements. */
	public boolean isEmpty();

	/** Removes the element at the
	 * specified position in this list. */
	public void remove(int index);

	/** Returns the number of elements in this list. */
	public int size();
}
